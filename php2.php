<!DOCTYPE html>
<!--
Ejercicio: php2.php
Enunciado: Hacer un programa que diga segun el color elegido entre unas opciones muestre una frase.
Ejecución: Si $color="verde" muestra la frase "verde como el trigo verde", si $color=amarillo, muestra la frase
"tengo un tractor amarillo", si $color=rosa, muestra "la pantera rosa", si $color tiene otro valor muestra, 
"Me gusta el arco iris".
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2, Dime un color</title>
    </head>
    <body>
        <?php
        $color=verde;
        switch ($color) {
            case verde:
                echo ("verde como el trigo verde");
                break;
            case amarillo:
                echo ("tengo un tractor amarillo");
                break;
            case rosa:
                echo ("la pantera rosa");
                break;
            default:
                echo ("Me gusta el arco iris");
                break;
}
        ?>
    </body>
</html>
