<!DOCTYPE html>
<!--
Ejercicio: php1.php
Enunciado: Hacer un programa que diga el número mayor de 3 números.
Ejecución: Si $a es mayor que $b se almacena en $mayor, si no se almacena el valor de $b,
si $a es mayor que $c se almacena en $mayor, si no se almacena el valor de $c.
-->


<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1, el mayor de 3 numeros</title>
    </head>
    <body>
        <?php
        $a = 3;
        $b = 4;
        $c = 2;
        $mayor = $a;

        if ($mayor < $b) {
            $mayor = $b;
        } else {
            if ($mayor < $c) {
                $mayor = $c;
            }
        }
        echo ("El número mayor es el ".$mayor);
        ?>
    </body>
</html>
