<!DOCTYPE html>
<!--
Ejercicio: php3.php
Enunciado: Hacer un programa que diga los números pares hasta el 100.
Ejecución: Si el modulo de la división entre 2 es 0, lo mostrará, si no lo es no lo mostrará.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3, Numeros pares hasta el 100</title>
    </head>
    <body>
        <?php
        for($i=1;$i<=100;$i++){
            if ($i%2==0){
                echo ($i." ");
            }
        }
        ?>
    </body>
</html>
